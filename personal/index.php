<?
define("NEED_AUTH", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Личный кабинет");
?>

<section class="personal">
	<div class="container">
		<div class="wrap">
			<?$APPLICATION->IncludeComponent(
	"bitrix:main.profile", 
	"profile", 
	array(
		"CHECK_RIGHTS" => "N",
		"SEND_INFO" => "N",
		"SET_TITLE" => "Y",
		"USER_PROPERTY" => array(
			0 => "UF_CITY",
			1 => "UF_REGION",
		),
		"USER_PROPERTY_NAME" => "",
		"COMPONENT_TEMPLATE" => "profile"
	),
	false
);?>
		</div>
	</div>
</section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>