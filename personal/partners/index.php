<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetTitle('Главная');
?>




<?php

function getChildren($user)
{
	$filter = Array
	(
		'>UF_LEFT' => $user['UF_LEFT'],
		'<UF_RIGHT' => $user['UF_RIGHT'],
		'UF_TREE' => $user['UF_TREE'],
		'UF_LEVEL' => $user['UF_LEVEL'] + 1
	);
	$rsUsers = CUser::GetList(($by="UF_LEFT"), ($order="asc"),
		$filter,
		array(
			"SELECT" => array(
				"UF_*",
			),
			"FIELDS" => array(
				'ID',
				'NAME',
				'EMAIL',
				'LOGIN',
				'LAST_NAME',
				'SECOND_NAME'
			),
		)
	);
	
	return $rsUsers;
}

function getRoots()
{
	$filter = Array
	(
		'UF_LEVEL' => 0
	);
	$rsUsers = CUser::GetList(($by="UF_LEFT"), ($order="asc"),
		$filter,
		array(
			"SELECT" => array(
				"UF_*",
			),
			"FIELDS" => array(
				'ID',
				'NAME',
				'EMAIL',
				'LOGIN',
				'LAST_NAME',
				'SECOND_NAME'
			),
		)
	);

	return $rsUsers;
}

function GetTree($rsUsers, $left = 0, $right = null, $lvl = 1)
{

	$tree = [];

	while($user = $rsUsers->GetNext())
	{
		if ($user['UF_LEFT'] >= $left + 1 && (is_null($right) || $user['UF_RIGHT'] <= $right) && $user['UF_LEVEL'] == $lvl)
		{
			$tree[$user['ID']] = $user;
			if ($children = getChildren($user))
			{
				$tree[$user['ID']]['items'] = GetTree($children, $user['UF_LEFT'], $user['UF_RIGHT'], $user['UF_LEVEL'] + 1);
			}
		}
	}

	return $tree;

}

function GetFullTree()
{
	
	$rsUsers = getRoots();

	$tree = [];

	while($user = $rsUsers->GetNext())
	{
		$tree[$user['ID']] = $user;
		if ($children = getChildren($user))
		{
			$tree[$user['ID']]['items'] = GetTree($children);
		}
	}

	return $tree;
}



?>

<div class="container">
	<div class="wrap">
		<?php
			// $tree = GetFullTree();
			global $USER;
			$user = CUser::GetByID($USER->GetID())->fetch();
			$res = getChildren($user);
			$tree = GetTree($res);
			
			printTree($tree);
		?>
	</div>
</div>

<?
	function printTree($tree)
	{
		echo '<ul>';
		foreach ($tree as $item)
		{
			$full_name = $item['LAST_NAME'] . ' ' . $item['NAME'] . ' ' . $item['SECOND_NAME'];
			echo '<li>' . $item['LOGIN'] . ' - ' . $full_name . '</li>';
			if (!empty($item['items']))
			{
				printTree($item['items']);
			}
		}
		echo '</ul>';
	}
?>
<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');
?>