<?
AddEventHandler("main", "OnBeforeUserAdd", array("User", "OnBeforeUserAddHandler"));
AddEventHandler("main", "OnBeforeUserDelete", Array("User", "OnBeforeUserDeleteHandler"));
AddEventHandler("main", "OnBeforeUserUpdate", Array("User", "OnBeforeUserUpdateHandler"));

//Bitrix\Main\Diag\Debug::writeToFile(array('user' => $user),"",NESTED_LOG);

define('NESTED_LOG', 'log/nested_set.log');
define('FILE', 'php_interface/init.php');

/**
 * Строим многоуровневую вложенность для пользовтелей (по партнеру)
 * по алгоритму NestedSets
 * Подробнее http://www.getinfo.ru/article610.html
 */
class User
{
	private static function GUID()
	{
		if (function_exists('com_create_guid') === true)
			return trim(com_create_guid(), '{}');

		$data = openssl_random_pseudo_bytes(16);
		$data[6] = chr(ord($data[6]) & 0x0f | 0x40);
		$data[8] = chr(ord($data[8]) & 0x3f | 0x80);
		return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
	}

	/**
	 * @param string $message
	 * @param array $params
	 */
	private static function ErrorLog($errorMessage, $params)
	{
		Bitrix\Main\Diag\Debug::writeToFile(
			array(
				'error_message' => $errorMessage,
				'params' => $params,
			),
			"",
			NESTED_LOG);
	}
		
	/**
	 * Смещаем значения ключей для ветки на (+2)
	 * @param integer $rightKey Правый ключ выбранного пользователя
	 * @param integer $tree Ветка выбранного пользователя
	 * @return bool
	 */
	private static function MoveUpKeys($rightKey, $tree)
	{
		$params = array('method' => 'MoveUpKeys', 'rightKey' => $rightKey, 'tree' => $tree);
		global $DB;
		// Обновляем родительскую ветку и узлы стоящие за родительским узлом
		if ($rightKey > 0 && strlen($tree) > 0)
		{
			$result = $DB->Query("UPDATE b_uts_user 
				SET UF_RIGHT = UF_RIGHT + 2, 
					UF_LEFT = IF(UF_LEFT > $rightKey, UF_LEFT + 2, UF_LEFT) 
				WHERE UF_RIGHT >= $rightKey
				AND UF_TREE = '$tree'");
			
			if ($result instanceof CDBResult)
			{
				return true;
			}
			else
			{
				self::ErrorLog('Ошибка в запросе метода MoveUpKeys()', $params);
				return false;
			}
		}
		self::ErrorLog('Неверные параметры', $params);
		return false;
	}

	/**
	 * Удалят дочерние узлы
	 */
/*	private static function RemoveNode($leftKey, $rightKey, $tree)
	{
		global $DB;
		// Подробнее http://www.getinfo.ru/article610.html
		// Обновляем родительскую ветку и узлы стоящие за родительским узлом
		if ($leftKey > 0 && $rightKey > 0 && $tree > 0)
		{
			$DB->Query("UPDATE b_uts_user 
				SET UF_LEFT = IF(UF_LEFT > $leftKey, UF_LEFT - ($rightKey - $leftKey + 1), UF_LEFT), UF_RIGHT = UF_RIGHT - ($rightKey - $leftKey + 1) 
				WHERE UF_RIGHT > $rightKey
				AND UF_TREE = $tree");
		}
			
	}
*/
	/**
	 * Если один из пользователей удалился или изменил партнера, то
	 * мы не удаляем его дочерних елементов, а только сдигаем ключи -2
	 * (Делаем обратное действие добавления).
	 * @param integer $leftKey
	 * @param integer $rightKey
	 * @param string $tree guid
	 * @return bool
	 *//*
	private static function MoveDownKeys($leftKey, $rightKey, $tree)
	{
		$params = array(
			'method' => 'MoveDownKeys',
			'leftKey' => $leftKey,
			'rightKey' => $rightKey,
			'tree' => $tree
		);

		global $DB;
		if ($rightKey > 0 && $tree > 0)
		{
			$result = $DB->Query("UPDATE b_uts_user 
				SET UF_LEFT = UF_LEFT - 2,
					UF_RIGHT = UF_RIGHT - 2 
				WHERE UF_LEFT > $rightKey 
				AND UF_TREE = $tree");
			
			if (!$result instanceof CDBResult)
			{
				self::ErrorLog('Ошибка в первом запросе MoveDownKeys', $params);
				return false;
			}

			// Обновляем родительскую ветку:
			$result = $DB->Query("UPDATE b_uts_user 
				SET UF_RIGHT = UF_RIGHT - 2 
				WHERE UF_RIGHT >= $rightKey 
				AND UF_LEFT < $rightKey 
				AND UF_TREE = $tree");
			
			if (!$result instanceof CDBResult)
			{
				self::ErrorLog('Ошибка во втором запросе MoveDownKeys', $params);
				return false;
			}
		}
			
	}
*/
	/**
	 * Добавляет пользователя в качестве первого дочернего элемента другого пользователя (партнера).
	 * @param CDBResult|Array $parent
	 * @param Array $arFields
	 * @return Array|false $arFields Если указан партнер, то возращаем массив $arFields. Иначе false
	 */
	private static function Prepend($parent, $arFields)
	{
		if ($parent instanceof CDBResult)
		{
			$parent = $parent->fetch();
		}
		if ($parent 
			&& is_array($parent) 
			&& $parent['UF_LEFT'] > 0 
			&& $parent['UF_RIGHT'] > 0 
			&& strlen($parent['UF_TREE']) > 0
			&& $parent['UF_LEVEL'] >= 0)
		{
			$left_key = $parent['UF_RIGHT'];
			$right_key = $parent['UF_RIGHT'] + 1;
			$tree = $parent['UF_TREE'];
			$level = $parent['UF_LEVEL'] + 1;

			if (!$result = self::MoveUpKeys($parent['UF_RIGHT'], $parent['UF_TREE'])) return false;

			$arFields['UF_LEFT'] = $left_key;
			$arFields['UF_RIGHT'] = $right_key;
			$arFields['UF_TREE'] = $tree;
			$arFields['UF_LEVEL'] = $level;
			return $arFields;
		}
		return false;
	}

	/**
	 * @param Array $arFields
	 * @return Array|bool
	 */
	private static function MakeRoot($arFields)
	{
		$leftKey = 1;
		$rightKey = 2;
		$level = 0;

		$tree = self::GUID();

		$arFields['UF_LEFT'] = $leftKey;
		$arFields['UF_RIGHT'] = $rightKey;
		$arFields['UF_TREE'] = $tree;
		$arFields['UF_LEVEL'] = $level;
		return $arFields;
	}

	private static function ChangePartnerForChildren($partnerId, $leftKey, $rightKey, $tree, $level)
	{
		$params = array(
			'method' => 'ChangePartnerForChildren',
			'partnerId' => $partnerId,
			'leftKey' => $leftKey,
			'rightKey' => $rightKey,
			'tree' => $tree,
			'level' => $level
		);

		global $DB;
		if ($partnerId > 0
			&& $leftKey > 0 
			&& $rightKey > 0 
			&& strlen($tree) > 0 
			&& $level > 0)
		{
			$result = $DB->Query("UPDATE b_uts_user 
				SET UF_PARTNER = $partnerId,
					UF_LEVEL = $level
				WHERE UF_LEFT > $leftKey
				AND UF_RIGHT < $rightKey
				AND UF_TREE = '$tree'");
			
			if (!$result instanceof CDBResult)
			{
				self::ErrorLog('Ошибка в запросе', $params);
				return false;
			}
			return true;
		}
		self::ErrorLog('Неверные параметры', $params);
		return false;
	}

	/**
	 * Обработчик события вызываемый перед добавлением пользователя.
	 * Если партнер указан то закрепялем пользователя дочерним.
	 * Иначе делаем корневым
	 */
	function OnBeforeUserAddHandler(&$arFields)
	{
		if ($arFields['UF_PARTNER'] > 0)
		{
			$parent = CUser::GetByID($arFields['UF_PARTNER']);

			if (!$arFields = self::Prepend($parent, $arFields))
			{
				global $APPLICATION;
				$APPLICATION->throwException("Что то пошло не так.");
				return false;
			}
		}
		elseif (!$arFields = self::MakeRoot($arFields))
		{
			global $APPLICATION;
			$APPLICATION->throwException("Что то пошло не так.");
			return false;
		}

		return $arFields;
	}

	/**
	 * Обработчик события вызываемый перед удалением пользователя.
	 * Если партнер является корневым и у него есть последователи, запрещаем удаление.
	 * Если пользователь дочерний, закрепляем всех его последователей за вышестоящим партнеромю
	 * @param integer $user_id Содержит id удаляемого пользователя
	 */
	function OnBeforeUserDeleteHandler($user_id)
    {
		global $DB;
		$user = CUser::GetByID($user_id)->fetch();

		if ($user && is_array($user) && $user['UF_LEVEL'] == 0)
		{
			// Если пользователь корневой для всех и у него есть последователи, запрещаем удаление
			$strSql = "SELECT VALUE_ID FROM b_uts_user WHERE UF_PARTNER=".intval($user_id);
			$rs = $DB->Query($strSql, false, "FILE: ".__FILE__."<br>LINE: ".__LINE__);
			
			if ($ar = $rs->Fetch()) 
			{
				global $APPLICATION;
				$APPLICATION->throwException("Удаление корневого пользователя запрещено.");
				return false;
			}
		}

		$parent = CUser::GetByID($user['UF_PARTNER'])->fetch();

		if ($user && $parent && is_array($parent)
			&& is_array($user) 
			&& $user['UF_LEFT'] > 0 
			&& $user['UF_RIGHT'] > 0 
			&& strlen($user['UF_TREE']) > 0)
		{
			$partnerId = $user['UF_PARTNER'];
			$leftKey = $user['UF_LEFT'];
			$rightKey = $user['UF_RIGHT'];
			$tree = $user['UF_TREE'];
			$level = $parent['UF_LEVEL'] + 1;
			$result = self::ChangePartnerForChildren($partnerId, $leftKey, $rightKey, $tree, $level);
			if (!$result)
			{
				global $APPLICATION;
				$APPLICATION->throwException("Что то пошло не так. Попробуйте повторить позже.");
				return false;
			}
			// self::MoveDownKeys($user['UF_LEFT'], $user['UF_RIGHT'], $user['UF_TREE']);
		}
	}
	
	/**
	 * Обработчик события вызываемый перед обновлением пользователя
	 * Если партнер изменился, то всем дочерним элементам устанавливаем вышестоящего партнера
	 */
	function OnBeforeUserUpdateHandler(&$arFields)
    {
		global $DB;
		$user = CUser::GetByID($arFields['ID'])->fetch();

		if (is_array($user) && $user['UF_LEVEL'] == 0)
		{
			// Если пользователь корневой для всех и у него есть последователи, запрещаем удаление
			$strSql = "SELECT VALUE_ID FROM b_uts_user WHERE UF_PARTNER=".intval($user['ID']);
			$rs = $DB->Query($strSql, false, "FILE: ".__FILE__."<br>LINE: ".__LINE__);
			
			if ($ar = $rs->Fetch()) 
			{
				global $APPLICATION;
				$APPLICATION->throwException("Изменение партнера корневого пользователя запрещено.");
				return false;
			}
		}

		if (strlen($arFields['UF_TREE']) <= 0)
		{
			$arFields['UF_TREE'] = self::GUID();
		}
		

		if (is_array($user) && $user['UF_PARTNER'] !== $arFields['UF_PARTNER'])
		{
			$oldPartner = CUser::GetByID($user['UF_PARTNER'])->fetch();

			if (is_array($oldPartner))
			{
				$partnerId = $user['UF_PARTNER'];
				$leftKey = $user['UF_LEFT'];
				$rightKey = $user['UF_RIGHT'];
				$tree = $user['UF_TREE'];
				$level = $oldPartner['UF_LEVEL'] + 1;
				$result = self::ChangePartnerForChildren($partnerId, $leftKey, $rightKey, $tree, $level);
				if (!$result)
				{
					global $APPLICATION;
					$APPLICATION->throwException("Что то пошло не так. Попробуйте повторить позже.");
					return false;
				}
				// self::MoveDownKeys($user['UF_LEFT'], $user['UF_RIGHT'], $user['UF_TREE']);
			}

			$newPartner = CUser::GetByID($arFields['UF_PARTNER'])->fetch();
			if (is_array($oldPartner))
			{

				if ($arFields = self::Prepend($newPartner, $arFields))
				{
					return $arFields;
				}
				else
				{
					global $APPLICATION;
					$APPLICATION->throwException("Что то пошло не так. Попробуйте повторить позже.");
					return false;
				}
			}
		}
		else
		{
			$arFields['UF_PARTNER'] = $user['UF_PARTNER'];
		}
    }
}
?>