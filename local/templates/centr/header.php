<? if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die(); ?>

<?php
$APPLICATION->RestartBuffer();
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">


	<link rel="shortcut icon" type="image/x-icon" href="<?=SITE_TEMPLATE_PATH?>/img/favicon.ico" />

	<?$APPLICATION->ShowMeta("robots")?>
	<?$APPLICATION->ShowMeta("keywords")?>
	<?$APPLICATION->ShowMeta("description")?>
	<title><?$APPLICATION->ShowTitle()?></title>
	<?$APPLICATION->ShowHead();?>

</head>
<body>
<div id="panel">
	<?$APPLICATION->ShowPanel();?></div>

	<header>
		<div class="container">
			<div class="wrap">
			  <div class="logo"><a href="/"><img src="<?=SITE_TEMPLATE_PATH?>/img/logo.png" alt=""></a></div>
				<nav>
					<ul>
						<li><a href="/about/">О проекте</a></li>
						<li><a href="/news/">Новости</a></li>
						<li><a href="/cities/">Города</a></li>
						<li><a href="/financial_plan/">Финансовый план</a></li>
					</ul>
				</nav>


				<a href="/personal/" class="account button">Личный кабинет</a>

			</div>
		</div>
	</header>
<main>
