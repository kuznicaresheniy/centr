
</main>

<footer>
  <div class="container">
    <div class="wrap">
      <div class="left">
        <span class="orange">
          Рады помочь 2018 г.
        </span>- центр услуг
        <div>
          <a href="/privacy" class="policy">Политика конфиденциальности</a>
        </div>
      </div>
      <div class="center">
        <img id="scroll_to_top"  style="display: block; cursor: pointer;" src="<?= SITE_TEMPLATE_PATH ?>/img/arrow2.png" alt="">
      </div>
      <div class="right">
        Разработка и сопровождение - <span class="orange"><a href="https://kuznica-resheniy.ru/" target="_blank">Кузница решений</a></span>
      </div>
    </div>
  </div>
</footer>

<?
CJSCore::Init(array("jquery"));
?>
<script src="<?= SITE_TEMPLATE_PATH ?>/js/js.js"></script>

</body>
</html>

