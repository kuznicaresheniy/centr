$(document).ready(function () {
	$('#registration_form').hide();

	$('.tabs button').on('click', function () {
		if (!$(this).is('.active')) {
			$(this).addClass('active');
			$(this).siblings().removeClass('active');
			if ($(this).is('.signUp')) {
				$('#authorization_form').hide();
				$('#registration_form').show();

				// $('#pass').after('  <input id="phone" type="tel" name="phone" placeholder="Телефон">');
				
				// $('#pass').before('<input id="email" type="email" name="email" placeholder="E-mail">');
				
				// $('#type').val('REGISTRATION');
				// $('.form [type=submit]').val('Зарегистрироваться');
			} else {
				$('#authorization_form').show();
				$('#registration_form').hide();


				
				// $('#email').remove();
				// $('#phone').remove();
				// $('#type').val('AUTH');
				// $('.form [type=submit]').val('Вход');
			}
			var controller = $(this).parents('form').attr('data-switch-action');
			$(this).parents('form').attr('data-switch-action', $(this).parents('form').attr('action'));
			$(this).parents('form').attr('action', controller);

		}
		// $(this).parents('form').submit();


		return false;
	});


	$('.advantage').on('click', function () {
		if ($(this).not('.active')) {
			$(this).siblings('.active').removeClass('active');
			$(this).addClass('active');
		}
	});

	$('form[action="/ajax/auth.php"]').on('submit', function () {
		var form = $(this);
		var data = $(this).serialize();
		var url = $(this).attr('action');

		$.ajax({
			type: "POST",
			url: url,
			data: data,
			timeout: 3000,
			error: function(request,error) {
                if (error == "timeout") {
                    alert('timeout');
                }
                else {
                    alert('Error! Please try again!');
                }
            },
			success: function (data) {
				data = JSON.parse(data);

				if (data['TYPE'] == "ERROR")
					alert(data['MESSAGE']);
				else
					window.location = '/';
			},
		});
		return false;
	});

	$('.advantage').on('click', function () {
		if ($(this).not('.active')) {
			$(this).siblings('.active').removeClass('active');
			$(this).addClass('active');
		}
	});


	$('#scroll_to_top').on('click', function () {
		$('body,html').animate({ scrollTop: 0 }, 300);
	});


	// Выбор городов для региона
	$('#select_region').change(function () {
        var regionId = $(this).val();
        $.ajax({
            type: 'POST',
            url: '/ajax/regionality.php',
            data: {
                'regionId': regionId
            },
            success: function (data) {
                data = JSON.parse(data);
                // the next thing you want to do 
                var $city = $('#select_city');
                $city.empty();
                console.log(data);
                if (!jQuery.isEmptyObject(data))
                {
                    for (var item in data) {
                        console.log(item);
                        $city.append('<option value=' + data[item]['ID'] + '>' + data[item]['NAME'] + '</option>');
                    }
                }
                else {
                    $city.append('<option selected disabled>Выберите город</option>');
                }
            }
        });
    
    });
	
});

