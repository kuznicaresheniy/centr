<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Главная");
?>


<?php
$arSelect = array(
	'ID', 'EMAIL', 'LOGIN', 'NAME', 'LAST_NAME'
);

$arFilter = array(
	'EMAIL' => 'originalzhenya@gmail.com'
);

$user = Bitrix\Main\UserTable::getList(array(
	"select" => $arSelect,
	"filter" => $arFilter
))->fetch();

$partnerId = null;
if (!empty($user)) {
	$partnerName = $user['LOGIN'];

	if (strlen($user['NAME']) > 0) {
		$partnerName = $user['NAME'] . ' ' . $user['LAST_NAME'];
	}
	$partnerId = $user['ID'];
}
?>


<?/* Авторизцаия и регистрация */ ?>

<section class="subheader">
	<div class="container">
		<div class="wrap">
			<h1>
			Этот проект изменит представление
			о рынке услуг России! <a href="/about/">Подробнее</a>
			</h1>
			<? if (!$USER->IsAuthorized()) : ?>
				<div class="form">
						<div class="tabs">
							<button class="signIn active">Вход</button>
							<button class="signUp">Регистрация</button>
						</div>
						<div class="auth_wrap">
							<form action="/ajax/auth.php" method="post" id="authorization_form">
								<div class="form-group">
									<input type="hidden" name="AUTH_FORM" value="Y">
									<input id="type" type="hidden" name="TYPE" value="AUTH">
									<input type="hidden" name="backurl" value="/">
									<input type="text" name="USER_LOGIN" maxlength="255" placeholder="Имя пользователя">
									<input type="password" name="USER_PASSWORD" maxlength="255" autocomplete="off" placeholder="Пароль">
									<input class="button" type="submit" name="Login" value="Войти">
								</div>
							</form>
							<form action="/ajax/auth.php" method="post" id="registration_form">
								<div class="form-group">
									<input type="hidden" name="TYPE" value="REGISTRATION"/>
									<input type="hidden" name="register_submit_button" value="Y"/>
									<input type="hidden" name="UF_PARTNER" value="<?= $partnerId ?>">
									<input type="text" name="REGISTER[LOGIN]" placeholder="Имя пользователя">
									<input type="text" name="REGISTER[EMAIL]" placeholder="E-Mail">
									<?/*
									<input type="password" name="REGISTER[PASSWORD]" maxlength="255" autocomplete="off" placeholder="Пароль">
									<input type="password" name="REGISTER[CONFIRM_PASSWORD]" maxlength="255" autocomplete="off" placeholder="Подтверждение пароля">
									*/?>
									<input type="text" name="REGISTER[PERSONAL_MOBILE]" placeholder="Номер телефона">
									<input class="button" type="submit" name="register_submit_button" value="Зарегистрироваться">
								</div>
							</form>
						</div>
				</div>
			<? endif; ?>
		</div>
	</div>
</section>
<section class="advantages">
  <div class="container">
    <h2>
      Уже <span class="orange">2864</span> человека в <span class="orange">108</span> городах<br>
признали наш проект уникальным и привлекательным!
    </h2>
    <div class="wrap" style="justify-content:space-around;">
      <div class="advantage active">
        <div class="bignumber">2864</div>
        <div class="measurement">человека</div>
        <div class="whatdid">Стали партнерами
проекта</div>
      </div>
      <div class="advantage">
        <div class="bignumber">108</div>
        <div class="measurement">городов</div>
        <div class="whatdid">Подключились к
системе</div>
      </div>
      <div class="advantage">
        <div class="bignumber">425621</div>
        <div class="measurement">рубль</div>
        <div class="whatdid">Выплачено
партнерам</div>
      </div>
      <div class="advantage">
        <div class="bignumber">1297500</div>
        <div class="measurement">рублей</div>
        <div class="whatdid">Инвестировано
партнерам</div>
      </div>
    </div>
    <div class="wrap">
      <img src="<?= SITE_TEMPLATE_PATH ?>/img/graph.png" alt="">
      <div class="text">
        За 3 месяца 2018 года<br>
в нашу команду<br>
вступило<br>
<span class="superbignumber">2864</span><br>
партнера<br>
      </div>
    </div>
  </div>
</section>
<section class="aboutUs">
  <div class="container">
    <h3>О проекте</h3>

    <div class="wrap" style="flex-direction:column">  <p>Новый проект, в планах которого изменить все представление о рынке услуг на территорий Российской Федерации. </p>
      <p>Была придумана система, в которой заказчики сами будут выбирать мастера по его заслугам и оценивать мастера тоже будут заказчики. Представьте, что вы выбираете мастера по рейтингу и отзывам других заказчиков, которые его вызывали. Вы можете объявить аукцион и выбрать тех, кто дают самую низкую цену. А можете найти мастера, кто находится рядом с вами и поможет вам сейчас. Я понимаю, что такой проект изменит весь рынок услуг России. Теперь мастера будут бояться нахамить, недоделать работу и уйти, оставить за собой мусор, или опоздать к назначенному времени. Все это будет отражаться на их рейтинге и отзывах.</p>
      <p>Мы нашли способ и систему как все это внедрить по всей России. Сейчас мы создаем партнерскую сеть. Мы ищем партнеров среди обычных граждан России. Мы хотим собрать команду, которая будет рекомендовать наш проект всем мастерам и заказчикам. Наша цель, чтобы наши партнеры зарабатывали вместе с нами и стали хозяевами своей жизни. Подробнее</p>
      </div>
  </div>
</section>
<section class="news">
  <div class="container">
    <h3>Новости компании</h3>

      

  <div class="wrap">
    <? $APPLICATION->IncludeComponent(
				"bitrix:news.list",
				"index_news",
				array(
					"ACTIVE_DATE_FORMAT" => "d.m.Y",
					"ADD_SECTIONS_CHAIN" => "Y",
					"AJAX_MODE" => "N",
					"AJAX_OPTION_ADDITIONAL" => "",
					"AJAX_OPTION_HISTORY" => "N",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "Y",
					"CACHE_FILTER" => "N",
					"CACHE_GROUPS" => "N",
					"CACHE_TIME" => "36000000",
					"CACHE_TYPE" => "A",
					"CHECK_DATES" => "Y",
					"DETAIL_URL" => "",
					"DISPLAY_BOTTOM_PAGER" => "Y",
					"DISPLAY_DATE" => "Y",
					"DISPLAY_NAME" => "Y",
					"DISPLAY_PICTURE" => "Y",
					"DISPLAY_PREVIEW_TEXT" => "Y",
					"DISPLAY_TOP_PAGER" => "N",
					"FIELD_CODE" => array(
						0 => "",
						1 => "",
					),
					"FILTER_NAME" => "",
					"HIDE_LINK_WHEN_NO_DETAIL" => "N",
					"IBLOCK_ID" => "1",
					"IBLOCK_TYPE" => "content",
					"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
					"INCLUDE_SUBSECTIONS" => "Y",
					"MESSAGE_404" => "",
					"NEWS_COUNT" => "3",
					"PAGER_BASE_LINK_ENABLE" => "N",
					"PAGER_DESC_NUMBERING" => "N",
					"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
					"PAGER_SHOW_ALL" => "N",
					"PAGER_SHOW_ALWAYS" => "N",
					"PAGER_TEMPLATE" => ".default",
					"PAGER_TITLE" => "Новости",
					"PARENT_SECTION" => "",
					"PARENT_SECTION_CODE" => "",
					"PREVIEW_TRUNCATE_LEN" => "",
					"PROPERTY_CODE" => array(
						0 => "",
						1 => "",
					),
					"SET_BROWSER_TITLE" => "Y",
					"SET_LAST_MODIFIED" => "N",
					"SET_META_DESCRIPTION" => "Y",
					"SET_META_KEYWORDS" => "Y",
					"SET_STATUS_404" => "N",
					"SET_TITLE" => "Y",
					"SHOW_404" => "N",
					"SORT_BY1" => "ACTIVE_FROM",
					"SORT_BY2" => "SORT",
					"SORT_ORDER1" => "DESC",
					"SORT_ORDER2" => "ASC",
					"STRICT_SECTION_CHECK" => "N",
					"COMPONENT_TEMPLATE" => "index_news"
				),
				false
			); ?>
  </div>
</section>
<section class="feedback">
  <div class="container">
    <h4>
      Не упусти шанс зарабатывать с нами!
    </h4>
    <div class="wrap">
      <? $APPLICATION->IncludeComponent(
	"bitrix:form.result.new", 
	"consult_request_ajax", 
	array(
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"CHAIN_ITEM_LINK" => "",
		"CHAIN_ITEM_TEXT" => "",
		"EDIT_URL" => "result_edit.php",
		"IGNORE_CUSTOM_TEMPLATE" => "N",
		"LIST_URL" => "result_list.php",
		"SEF_FOLDER" => "",
		"SEF_MODE" => "Y",
		"SUCCESS_URL" => "",
		"AJAX_MODE" => "Y",
		"AJAX_OPTION_SHADOW" => "N",
		"AJAX_OPTION_JUMP" => "Y",
		"AJAX_OPTION_STYLE" => "Y",
		"USE_EXTENDED_ERRORS" => "N",
		"WEB_FORM_ID" => "1",
		"COMPONENT_TEMPLATE" => "consult_request_ajax"
	),
	false
); ?>
    </div>
  </div>
</section>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>