<?
require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");

if (!CModule::IncludeModule("iblock")) die();

$sectionId = $_POST['regionId'];

$arSelect = array("ID", "NAME");
$arFilter = array('IBLOCK_CODE' => 'regions', "ACTIVE" => "Y", "SECTION_ID" => $sectionId);
$res = CIBlockElement::GetList(array(), $arFilter, false, array(), $arSelect);

$arRegions = array();
while ($region = $res->GetNextElement()) {
    $arFields = $region->GetFields();
    $arRegions[$arFields['ID']]['ID'] = $arFields['ID'];
    $arRegions[$arFields['ID']]['NAME'] = $arFields['NAME'];
}

echo json_encode($arRegions);


require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
?>