<?
require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");
use Bitrix\Main\Mail\Event;


switch ($_REQUEST['TYPE']) {
	case "AUTH":
		{
			$APPLICATION->IncludeComponent("bitrix:system.auth.form", "ajax", array(
				"REGISTER_URL" => "",
				"FORGOT_PASSWORD_URL" => "",
				"PROFILE_URL" => "/",
				"SHOW_ERRORS" => "Y"
			));
			break;
		}
	case "REGISTRATION":
		{
			$arSelect = array(
				'ID', 'EMAIL', 'LOGIN', 'NAME', 'LAST_NAME'
			);

			$arFilter = array(
				'EMAIL' => 'originalzhenya@gmail.com'
			);

			$user = Bitrix\Main\UserTable::getList(array(
				"select" => $arSelect,
				"filter" => $arFilter
			))->fetch();

			$partnerId = null;
			if (!empty($user)) {
				$partnerName = $user['LOGIN'];

				if (strlen($user['NAME']) > 0) {
					$partnerName = $user['NAME'] . ' ' . $user['LAST_NAME'];
				}
				$partnerId = $user['ID'];
			}


			$login = $_POST['REGISTER']['LOGIN'];
			$email = $_POST['REGISTER']['EMAIL'];
			$phone_number = $_POST['REGISTER']['PERSONAL_MOBILE'];
			$partner_id = $partnerId;

			$password_chars = array(
				"abcdefghijklnmopqrstuvwxyz",
				"ABCDEFGHIJKLNMOPQRSTUVWXYZ",
				"0123456789",
			);

			$NEW_PASSWORD = $NEW_PASSWORD_CONFIRM = randString(8, $password_chars);
			$arFields = array(
				"LOGIN" => $login,
				"PASSWORD" => $NEW_PASSWORD,
				"CONFIRM_PASSWORD" => $NEW_PASSWORD_CONFIRM,
				"EMAIL" => $email,
				"ACTIVE" => "Y",
				"LID" => "ru",
				"ACTIVE" => "Y",
				"GROUP_ID" => array(10, 11),
				"UF_PARTNER" => $partnerId
			);

			$user = new CUser;
			$UserID = $user->Add($arFields);

			if (intval($UserID) > 0)
			{
				$send_result = Event::send(array(
					"EVENT_NAME" => "NEW_USER",
					"LID" => "s1",
					"C_FIELDS" => array(
						"USER_ID" => $UserID,
						"EMAIL" => $email,
						"LOGIN" => $login,
						"PASSWORD" => $NEW_PASSWORD,
					),
				));
				$USER->Authorize($UserID);
				$result['ERROR_TYPE'] = 'LOGIN';
				$result['TYPE'] = 'SUCCESS';
				$result['MESSAGE'] = 'Пользователь успешно добавлен.';
			}
			else
			{
				$result['TYPE'] = 'ERROR';
				$result['MESSAGE'] = HTMLToTxt($user->LAST_ERROR);
			}

			echo json_encode($result);

			die();


			/*
			$APPLICATION->IncludeComponent(
				"bitrix:main.register",
				"ajax",
				array(
					"SHOW_FIELDS" => array("LOGIN", "EMAIL", "PERSONAL_MOBILE", "UF_PARTNER"),
					"REQUIRED_FIELDS" => array("LOGIN", "EMAIL"),
					"AUTH" => "Y",
					"USE_BACKURL" => "Y",
					"SUCCESS_PAGE" => "",
					"SET_TITLE" => "N",
					"USER_PROPERTY" => array(),
					"USER_PROPERTY_NAME" => "",
				)
			);
			*/
		}
}

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
?>